from typing import Dict, Type

from django.urls import reverse, reverse_lazy
from django import forms
from django.views import generic

from .models import Book


class BookCreateView(generic.CreateView):
    """書籍登録ビュー"""
    model = Book
    fields = ('id', 'name', 'publisher', 'isbn', 'published_at', 'price',
              'department',)

    def get_form(self, form_class: Type[forms.Form] = None) -> forms.Form:
        """書籍登録ビューのフォームを返却する。

        Args:
            form_class: フォームクラス。
        Returns:
            書籍登録ビューのフォーム。
        """
        form = super().get_form(form_class)
        form.fields['id'].widget = forms.HiddenInput()  # 隠されたフィールドに設定
        form.fields['department'].empty_label = None  # 未入力項目を非表
        return form

    def get_context_data(self, **kwargs: Dict) -> Dict:
        """テンプレートをレンダリングするときに使用するデータを格納した辞書を返却する。

        Args:
            **kwargs: コンテキストに登録するデータを格納した辞書。
        Returns:
            テンプレートをレンダリングするときに使用するデータを格納した辞書。
        """
        ctx = super().get_context_data(**kwargs)
        ctx['submit_name'] = 'register'
        ctx['submit_value'] = '登録'
        return ctx

    def get_success_url(self) -> str:
        """書籍登録後に遷移するページのURIを返却する。

        Returns:
            書籍登録後に遷移するページのURI。
        """
        return reverse('books:book-detail', kwargs={'pk': self.object.id})


class BookListView(generic.ListView):
    """書籍リストビュー"""
    model = Book


class BookDetailView(generic.DetailView):
    """書籍詳細ビュー"""
    model = Book


class BookUpdateView(generic.UpdateView):
    """書籍更新ビュー"""
    model = Book
    fields = ('name', 'publisher', 'isbn', 'published_at', 'price',
              'department',)

    def get_form(self, form_class=None) -> forms.Form:
        """書籍登録ビューのフォームを返却する。

        Args:
            form_class: フォームクラス。
        Returns:
            書籍登録ビューのフォーム。
        """
        form = super().get_form(form_class)
        form.fields['department'].empty_label = None  # 未入力項目を非表示
        return form

    def get_context_data(self, **kwargs: Dict) -> Dict:
        """テンプレートをレンダリングするときに使用するデータを格納した辞書を
           返却する。

        Args:
            **kwargs: コンテキストに登録するデータを格納した辞書。
        Returns:
            テンプレートをレンダリングするときに使用するデータを格納した辞書。
        """
        ctx = super().get_context_data(**kwargs)
        ctx['submit_name'] = 'update'
        ctx['submit_value'] = '更新'
        return ctx

    def get_success_url(self) -> str:
        """書籍更新後に遷移するページのURIを返却する。

        Returns:
            書籍更新後に遷移するページのURI。
        """
        return reverse('books:book-detail', kwargs={'pk': self.object.id})


class BookDeleteView(generic.DeleteView):
    """書籍削除ビュー"""
    model = Book
    success_url = reverse_lazy('books:book-list')
