from django.urls import path

from . import views

app_name = 'books'

urlpatterns = [
    path('', views.BookListView.as_view(), name='book-list'),
    path('detail/<uuid:pk>/', views.BookDetailView.as_view(),
         name='book-detail'),
    path('update/<uuid:pk>/', views.BookUpdateView.as_view(),
         name='book-update'),
    path('delete/<uuid:pk>/', views.BookDeleteView.as_view(),
         name='book-delete'),
    path('create/', views.BookCreateView.as_view(), name='book-create'),
]
