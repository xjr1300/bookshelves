import uuid

from django.db import models

from departments.models import Department


class Book(models.Model):
    """書籍モデルクラス"""
    id = models.UUIDField('ID', primary_key=True, default=uuid.uuid4)
    name = models.CharField('書籍名', max_length=100)
    publisher = models.CharField('出版社', max_length=100, null=True,
                                 blank=True)
    isbn = models.CharField('ISBN', max_length=30, null=True, blank=True)
    published_at = models.DateField('発売日', null=True, blank=True)
    price = models.IntegerField('税抜き価格')
    department = models.ForeignKey(Department, db_column='部署ID',
                                   on_delete=models.CASCADE,
                                   verbose_name='部署')
    registered_at = models.DateTimeField('更新日時', auto_now_add=True)
    updated_at = models.DateTimeField('更新日時', auto_now=True)

    def __str__(self) -> str:
        """書籍名を返却する。

        Returns:
            書籍名。
        """
        return self.name

    class Meta:
        db_table = 'books'  # データベーステーブル名。
        verbose_name = verbose_name_plural = '書籍'
