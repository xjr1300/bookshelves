from django.contrib import admin

from .models import Book


class BookAdmin(admin.ModelAdmin):
    fields = ('name', 'publisher', 'isbn', 'published_at', 'price',
              'department',)
    list_display = fields + ('registered_at', 'updated_at',)


admin.site.register(Book, BookAdmin)
