# DjangoによるWebアプリ開発

今回は、`Python`で実装されたフルスタック`Web`フレームワークである`Django`を使用して、部署ごとに書籍を管理する`Web`アプリ(書籍管理アプリ)を開発します。

フルスタックとは、ユーザー認証、データベースアクセスや入力データの検証など、多数の高度な機能を備えていることを示します。

また、`Django`には、空間データを扱うライブラリなど、`Django`を拡張する多種、多様なライブラリが多く開発され公開されています。
これらのライブラリを利用することで、`Web`アプリを迅速に開発して公開できます。

今回、開発する書籍管理アプリのソースコードは、[ここ](https://bitbucket.org/xjr1300/bookshelves/src/master/)にあります。
書籍管理アプリが正常に動作しない場合は、リンク先のソースコードを確認してください。

なお、今回で開発する`Web`アプリは、`Django`が提供するシンプルな開発用の`Web`サーバーで動作するため、セキュリティ上の問題からインターネットに公開しないでください。

## 1. 準備

下記をパソコンにインストールしてください。

- `Python3`
- `Visual Studio Code`
- `Visual Studio Code`の`Python`拡張機能のインストール

インストール方法は、
[PythonによるExcel操作](https://drive.google.com/open?id=1686xJCUHxRVClZrpxiyV0fMNXkvJkU_J&authuser=0)を
参照してください。

## 2. Djangoとは

### 2.1. Djangoの概要

[Djangoホームページ](https://www.djangoproject.com/,"Django")

`Django`（じゃんご）は、`Python`で実装されたWebアプリケーションフレームワークです。
`Django`は、オープンソースとして開発及び公開されています。
`Django`は、`Web`アプリ開発で必要な部品を多数備えており、
`Web`アプリ開発にありがちな単純または退屈な作業を少なくします。

`Django`を使えば、コンテンツ管理システムや`Wiki`からソーシャルネットワーク、ニュースサイトなど、高品質な`Web`アプリを簡単に、少ないコードで開発できます。
シンプルな`Web`アプリケーションであれば、数時間で開発することも可能です。
また、必要に応じて機能を拡張すれば、複雑な`Web`アプリを作成できます。

`Django`による`Web`アプリ開発には、下記の利点があります。

- 高速な動作
- フルスタック・フレームワーク
- セキュリティ的に安全な設計
- メンテナンスの容易さ
- 自由に選べるプラットフォーム
- 学習コストの低さ

`Youtube`、`Dropbox`、`Instagram`などは、`Django`で開発されたアプリケーションの代表です。
ちなみに`Python`の生みの親である`Guido van Rossum`氏は、2013年から2019年くらいまで`Dropbox`社に所属していました。
現在(2021年1月時点)は、マイクロソフト社に所属しています。
なお、`Guido van Rossum`氏は、`Django`の開発にかかわっていません。


### 2.2. MVTフレームワーク

`Django`は`MVT`フレームワークを採用しています。

| `M` / `V` / `T` | 説明 |
| --- | --- |
| `M` | `モデル`を意味しており、アプリが扱うデータとその処理を実装します。 |
| `V` | `ビュー`を意味しており、ユーザーからのリクエストごとにアプリの動作を実装します。|
| `T` | `テンプレート`を意味しており、ユーザーに返却するレスポンスの雛形を示します。 |

### 2.3. テンプレート

`Django`における`テンプレート`は、クライアントに返却するレスポンスの雛形となるテキストファイル(単なるテキストを含む)を示します。

`Django`には、`テンプレート`をレンダリングする機能があります。
`テンプレート`のレンダラーに`テンプレート`とデータ(`コンテキスト`)を渡すと、
`コンテキスト`の内容で`テンプレート`がレンダリングされ、レスポンスとなる`HTML`やメール本文などのテキストが生成されます。

Webシステムの場合、`Django`がレンダリングした`HTML`がクライアントであるWebブラウザに返却され、
WebブラウザにWebページが表示されます。

## 2.4. ORM

`Django`には、`ORM(Object-relational mapping)`と呼ばれる、モデルとデータベースを連携する機能があります。
このため、`Django`ではデータベースを操作する`SQL`文を記述することなしに、下記のことができます。

- データ定義(`DDL`系)
    - モデルからテーブルの作成
    - モデルの変更をテーブルに反映
    - 削除されたモデルに対応するテーブルの削除など
- データ操作(`DML`系)
    - テーブルへのレコード登録
    - テーブルレコードの更新
    - テーブルレコードの削除
    - レコードの問い合わせ
    - テーブルの結合
    - 集計など

## 4. Python仮想環境の作成

書籍管理アプリ用の仮想環境を、`C:\Python\venv\bookshelves`ディレクトリに作成します。
`Visual Studio Code`の起動、ターミナルの表示やターミナルを`Windows PowerShell`にする方法は、
[PythonによるExcel操作](https://drive.google.com/open?id=1686xJCUHxRVClZrpxiyV0fMNXkvJkU_J&authuser=0)
を参照してください。

`Visual Studio Code`のターミナル(`Windows PowerShell`)に下記の通り入力して、書籍管理アプリ用の仮想環境を作成します。

```powershell
# `python`コマンドで呼び出されるPythonのバージョンを確認
# Python 3.8.3など、バージョン3系であることを確認
python -V
# Dドライブにカレントディレクトリを移動
cd C:\
# Python仮想環境ディレクトリを作成
mkdir -p .\Python\venv\
# Python仮想環境ディレクトリにカレントディレクトリを移動
cd .\Python\venv\
# 書籍管理アプリ用のPython仮想環境を構築
python -m venv bookshelves
```

## 5. Djangoのインストール

ターミナルに下記の通り入力して、書籍管理アプリ用の`Python`仮想環境を有効にするとともに、`Django`をインストールします。

**なお、以降は、すべて書籍管理アプリ用の`Python`仮想環境を有効にして作業します。**

```powershell
# 書籍管理アプリ用のPython仮想環境を有効化
.\bookshelves\Scripts\activate
# Djangoのインストール
pip install django
```

## 6. 書籍管理アプリプロジェクトの作成

`Django`は、開発する`Web`アプリをプロジェクトとして管理します。

`Django`プロジェクトには、複数の「アプリ」を追加できます。
ただし、`Django`における「アプリ」は、一般的なアプリと異なり「アプリ」単独では動作しません。
`Django`における「アプリ」は、特定の機能を持った塊を意味しており、`Django`でショッピングサイトを開発する場合は、下記アプリでプロジェクトが構成されるかもしれません。

- 顧客管理アプリ
- 商品管理アプリ
- カタログアプリ
- 購買管理アプリ
- 商品配送アプリ
- ...

`Django`は、単独のアプリでプロジェクトを構成するよりも、機能などで区分された複数のアプリで構成することを推奨しています。

ターミナルに下記を入力して、書籍管理アプリのプロジェクトを作成します。

```powershell
# カレントディレクトリをDドライブルートに移動
cd D:\
# プロジェクトディレクトリの作成
mkdir Projects
# カレントディレクトリをプロジェクトディレクトリに移動
cd .\Projects
# 書籍管理アプリプロジェクトの作成
django-admin startproject bookshelves
```

**以降、`D:\Projects\bookshelves`ディレクトリを`プロジェクトディレクトリ`と呼びます。**

## 7. Visual Studio Codeでプロジェクトディレクトリを開く

`Visual Studio Code`の`[ファイル]-[フォルダーを開く...]`で`プロジェクトディレクトリ`を開きます。
その後、`[ファイル]-[名前を付けてワークスペースを保存...]`で、`プロジェクトディレクトリ`にワークスペースを`bookshelves.code-workspace`ファイルに保存します。

`プロジェクトディレクトリ`には、下記のディレクトリとファイルがあります。

```powershell
D:\Projects\bookshelves
├─bookshelves/
│    └─__init__.py
│    └─asgi.py
│    └─settings.py
│    └─urls.py
│    └─wsgi.py
└─manage.py
```

`プロジェクトディレクトリ`の`manage.py`ファイルは、プロジェクトを管理するコマンドを実行するプログラムです。
`manage.py`で下記を実行できます。

- データベースのマイグレート(モデルの実装内容をデータベースに適用)
- 開発用`Web`サーバーの起動
- データ(フィクチャー)のロード
- ユニットテストの実行など

**以降、`プロジェクトディレクトリ`の`bookshelves`ディレクトリを、`プロジェクト設定ディレクトリ` と呼びます。**

`プロジェクトディレクトリ(bookshelves\)`と`プロジェクト設定ディレクトリ(bookshelves\bookshelves\)`で、
`bookshelves`ディレクトリが2段になっていることに注意してください。

**以降、ファイルやディレクトリのパスは、`プロジェクトディレクトリ`からの相対パスで表記します。**

## 8. Visual Studio Codeの設定

### 8.1. Visual Studio Codeのテキストエディタ設定

`F1`キーを押して表示された`コマンドパレット`に`settings`と入力します。

![ユーザー設定を開く](images/vscode_open_user_settings.png)

表示された`基本設定: ユーザー設定を開く(Preferences: Open User Settings)`をクリックします。

#### 8.2. タブのスペース展開設定

表示された`[設定の検索]`ボックスに`indent`と入力して、`Editor: Insert Spaces`をチェックして、`Editor: Tab Size`に`4`を入力します。
これにより、`Tab`キーを押したとき、4つのスペースが代わりにテキストファイルに入力されます。
環境によってタブのインデント幅が異なるため、ソースコードにタブを入力することが、あまり推奨されていません。

![インデント設定](images/vscode_tab_to_four_spaces.png)

#### 8.3. テキストファイルのエンコーディング設定

表示された`[設定の検索]`ボックスに`encoding`と入力して、`Files: Encoding`を`utf8`にします。
これにより、テキストファイルは`UTF-8`に符号化されて保存されます。
`Python`ソースコードのデフォルトエンコーディングは`UTF-8`なので、これに習っています。
なお、日本語`Windows`のテキストファイルのエンコーディングは、`Shift-JIS`を拡張した`CP932`です。

#### 8.4. 改行コードの設定

表示された`[設定の検索]`ボックスに`eol`と入力して、`Files: Eol`を`\n`にします。
これにより、テキストファイルの改行コードが`\n`になります。
なお、日本語`Windows`のテキストファイルの改行コードは`\r\n`です。

### 8.5. Visual Studio Codeに仮想環境のパスを設定

`F1`キーを押して表示された入力ボックスに`settings`と入力します。
表示された`基本設定: ユーザー設定を開く(Preferences: Open User Settings)`をクリックします。

表示された`設定`タブの入力ボックスに`venv`を入力します。
表示された`Python: Venv Path`に、仮想環境を作成したディレクトリのパス(`C:\Python\venv`)を入力します。

![仮想環境パスの設定](images/vscode_set_venv_path.png)

その後、`F1`キーを再度押し、`コマンドパレット`に`reload`を入力後、`開発者: ウィンドウの再読み込み(Developer: Reload Window)`をクリックして、`Visual Studio Code`をリロードします。

### 8.6. Visual Studio CodeのPython開発環境の確認

`[表示]-[エクスプローラー]`をクリックして、表示された`エクスプローラー`で`manage.py`ファイルをダブルクリックして開きます。

`Visual Studio Code`のステータスバーに表示されている`Python`とバージョン番号をクリックします。ステータスバーが表示されていない場合は、`[表示]-[外観]-[ステータスバーを表示]`をクリックします。
`Python`のバージョン番号が表示されていない場合は、ステータスバーを右クリックして、`[Python(拡張機能)]`をクリックします。

![Python拡張機能](images/python_extension.png)

`コマンドパレット`から`Enter interpreter path...`をクリックして、その後`Find...`をクリックします。

![インタープリターパスの入力](images/enter_interpreter_path.png)

![インタープリターのブラウズ](images/browse_interpreter.png)

表示された`[Select Python interpreter]ダイアログ`に、先ほど作成した書籍管理用の`Python`仮想環境にある`Python`実行ファイルのパス`C:\Python\venv\bookshelves\Scripts\python.exe`を入力して、`[インタープリターを選択]ボタン`をクリックします。

ステータスバーに、`Python 3.x.x x-bit ('bookshelves')`などと表示されていることを確認してください。

### 8.7. ターミナルの再起動

現在開いているターミナルは、書籍管理アプリ用の`Python`仮想環境が有効になっていません。
ターミナルをすべて閉じて、`[ターミナル]-[新しいターミナル]`をクリックしてください。
ターミナルのプロンプトに`(bookshelves)`と表示されていれば、書籍管理アプリ用の`Python`仮想環境が有効になっています。

**以降は、書籍管理アプリ用の`Python`仮想環境が有効化されたターミナル(`Windows PowerShell`)で作業します。**

**ターミナルが書籍管理アプリ用の`Python`仮想環境が有効になっていない、またカレントディレクトリが`プロジェクトディレクトリ`と異なる場合、コマンドなどの実行に失敗します。**

![書籍管理アプリ用Python仮想環境](images/venv_activated.png)

## 9. プロジェクトの設定

`Visual Studio Code`の`エクスプローラー`で`プロジェクトディレクトリ`の下にある`bookshelves/settings.py`ファイルをダブルクリックして開き、下記の通り編集して、ファイルを上書き保存します。

テキストファイルの内容を編集する場合、下記の通り編集します。

- 行の先頭に`+`が表示されている場合、その行をファイルに追加します。
- また、行の先頭に`-`が表示されている場合、その行をファイルから削除します。
- 前の行の先頭が`-`で、次の行が`+`の場合、`-`の行を`+`の行で入れ替えします。

```python
# bookshelves/settings.py

# 108行目くらい
- LANGUAGE_CODE = 'en-us'
+ LANGUAGE_CODE = 'ja'      # 言語を日本語に設定
# 110行目くらい
- TIME_ZONE = 'UTC'
+ TIME_ZONE = 'Asia/Tokyo'  # タイムゾーンを東京に設定
```

## 10. データベースのマイグレーション

ターミナルに下記を入力して、書籍管理アプリが使用するデータベースを準備(マイグレート)します。

```powershell
python manage.py migrate
```

データベースをマイグレートすると、`プロジェクトディレクトリ`に`db.sqlite3`ファイルが作成されます。
このファイルは、書籍管理アプリが使用するデータベースです。

書籍管理アプリが使用する`DBMS(データベース管理システム)`は`SQLite3`です。
`SQLite3`は、軽量なデータベースで、スマホアプリのデータ保存などで利用されています。

なお、`Django`は、`SQLite3`の他に`PostgreSQL`、`MySQL`、`SQL Server`や`Oracle`などの`DBMS`を利用できます。

## 11. Django開発用Webサーバーの起動

現在開いているターミナルとは別のターミナルを起動します。
起動したターミナルに下記を入力して、`Django`が提供する開発用`Web`サーバーを起動します。
なお、このターミナルは`Django`の開発用`Web`サーバーを起動している間、使用できません。

`Django`を利用した`Web`アプリは、通常、この`Django`の開発用`Web`サーバーを起動した状態で実装します。

```powershell
python manage.py runserver
```

`Web`ブラウザのアドレスに`http://127.0.0.1:8000/`を入力後、`Enter`キーを押下して、下記`Web`ページが表示されることを確認します。

![Django開発用Webサーバー](images/development_server.png)

`Django`の開発用`Web`サーバーを停止する場合は、`Django`の開発用`Web`サーバーを起動したターミナルで`Ctrl+C`を押下します。

## 12. 部署アプリの作成

ターミナルに下記を入力して、書籍の管理単位である部署を管理する部署アプリ(`departments`)を作成します。
部署アプリを作成すると、`プロジェクトディレクトリ`に部署アプリ用の`departments`ディレクトリが作成されます。

```powershell
python manage.py startapp departments
```

### 12.1. プロジェクトに部署アプリを登録

`bookshelves/settings.py`を下記の通り編集後、ファイルを上書き保存して、プロジェクトに書籍アプリを登録します。

```python
# bookshelves/settings.py
  INSTALLED_APPS = [
      'django.contrib.admin',
      'django.contrib.auth',
      'django.contrib.contenttypes',
      'django.contrib.sessions',
      'django.contrib.messages',
      'django.contrib.staticfiles',
+     'departments',  # 追加
  ]

```

### 12.2. 部署モデルの実装

`Django`では、モデルをアプリディレクトリの`models.py`ファイルに実装します。

`Visual Studio Code`の`エクスプローラー`で`departments`ディレクトリ内の`models.py`ファイルをダブルクリックして開き、そのファイルに下記を記述して、ファイルを上書き保存します。

```python
# departments/models.py
from django.db import models

class Department(models.Model):
    """部署モデルクラス"""
    name = models.CharField('部署名', max_length=80)

    class Meta:
        verbose_name = verbose_name_plural = '部署'
        ordering = ('id',)

    def __str__(self) -> str:
        """部署名を返却する。

        Returns:
            部署名。
        """
        return self.name

```

- 部署モデルに部署名フィールド(`name`)を定義しています。
    - 部署名フィールドに記録できる文字数は、`max_length`で指定した80文字までです。
- `print`コマンドなどでモデルインスタンスを出力するときに、出力する文字列を`__str__`メソッドで返却しています。

モデルクラスを実装した後、データベースをマイグレーションすると、データベースにモデルインスタンスのデータを格納するテーブルが作成されます。

`Django`は`primary_key=True`としたフィールドが存在しない場合、主キーとなる整数を記録する`id`フィールドを自動的に追加します。
よって、部署モデルは`id`と`name`フィールドを持ちます。

主キーとは、テーブル内でレコード(データ)を一意に識別するための値または 値の組です。
`Django`は主キーを値の組で構成する複合主キーを実装することはできません。
`Django`において主キーは、常に1つのフィールドで構成されます。

### 12.3. 部署テーブルの作成

ターミナルで`makemigrations`コマンドを実行して、部署アプリに実装されたモデルの内容で、
データベースをマイグレート(意訳：変更）するマイグレーションファイルを作成します。
`makemigrations`コマンドを実行すると、アプリごとに採番されたマイグレーション番号(0001)が表示されます。

```powershell
python manage.py makemigrations departments
```

![部署アプリのマイグレーションファイル作成](images/create_departments_migration_file.png)

ターミナルで`sqlmigrate`コマンドを実行して、作成したマイグレーションファイルの内容を確認します。
`CREATE TABLE`文が記録され部署テーブル(departments_department)が作成されることを示します。

マイグレーションファイルの内容を確認する必要がない場合、`sqlmigrate`コマンドの実行を省略することができます。

`Django`はモデルインスタンスのデータを記録するテーブルの名前を`<app_name>_<model_name>`の書式で決定します。
`<app_name>`や`<model_name>`の大文字は小文字に変換されます。

部署モデルの場合、アプリ名が`departments`でモデル名が`Department`であるため、部署モデルインスタンスのデータを記録するテーブルの名前は、`departments_department`になります。

なお、モデルインスタンスのデータを記録するテーブルの名前は変更可能です。

```powershell
python manage.py sqlmigrate departments 0001
```

![部署アプリのマイグレート確認](images/check_departments_migration.png)

ターミナルで`migrate`コマンドを実行して、作成したマイグレーションファイルをデータベースに適用します。
これによりデータベースに部署テーブル(`departments_department`)が作成されます。

```powershell
python manage.py migrate departments 0001
```

![部署アプリのマイグレート](images/migrate_departments.png)

**`Django`では、モデルを変更するたびに、上記の手順で`makemigrations`コマンドでマイグレーションファイルを作成した後、`migrate`コマンドでデータベースに作成したマイグレーションファイルをデータベースに適用する必要があります 。**

`makemigrations`コマンドは、アプリ名を指定しない場合、すべてのアプリのモデルの変更を確認して、変更をデータベースに適用するマイグレーションファイルをアプリごとに作成します。

`migrate`コマンドは、アプリ名を指定しない場合、すべてのアプリのマイグレーションファイルを確認して、適用していないマイグレーションファイルをデータベースに適用します。

また、上記`departments 0001`のようにアプリ名とマイグレーション番号を指定した場合、マイグレーション番号が一致する当該アプリのマイグレーションファイルをデータベースに適用します。

### 12.4. Django Shellによる部署の登録

ターミナルで`shell`コマンドを実行して、`Python`インタラクティブシェルで部署モデルインスタンスを登録します。
最後の`exit()`で`Python`インタラクティブシェルを終了します。

```powershell
python manage.py shell  # Pythonインタラクティブシェルをターミナルに起動
```

```powershell
>>> from departments.models import Department
>>> names = ['交通基盤計画部', '橋梁構造部', '河川・地域環境部', '海外部', 'ICT開発室']
>>> for name in names:  # namesリストの要素を順番にname変数に設定
...     department = Department(name=name)  # 部署モデルインスタンスを構築
...     department.save()   # 部署をデータベースに保存
...
>>> Department.objects.all()    # データベースに登録された部署を表示
<QuerySet [<Department: 交通基盤計画部>, <Department: 橋梁構造部>,
<Department: 河川・地域環境部>, <Department: 海外部>, <Department: ICT開発室>]>
>>> exit()  # Pythonインタラクティブシェルを終了
```

上記により、コンサルタント事業部の部署がデータベースに登録されます。
なお、部署の`id`は1からの連番で採番されます。
下記図の`rowid`は`SQLite`が自動的に採番したテーブル内でユニークな値です。
ちなみに、`SQLite`は`rowid`のないテーブルを作成することができます。

![データベースに登録された部署](images/department_rows.png)

## 13. 書籍アプリの作成

ターミナルで`startapp`コマンドを実行して、書籍を管理する書籍アプリ(`books`)を作成します。
書籍アプリを作成すると、`プロジェクトディレクトリ`に書籍アプリ用の`books`ディレクトリが 作成されます。

```powershell
python manage.py startapp books
```

### 13.1. プロジェクトに書籍アプリを登録

部署アプリと同様に`プロジェクト設定ファイル(bookshelves/settings.py`)を下記の通り編集後、ファイルを上書き保存して、プロジェクトに書籍アプリを登録します。

```python
# bookshelves/settings.py
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'departments',
+   'books',  # 追加
]
```

### 13.2. 書籍モデルの実装

書籍モデルを`books/models.py`ファイルに下記の通り実装します。
書籍モデルは部署モデルと下記が異なります。

- `id`フィールドを明示的に実装して、`UUID4`と呼ばれる他と衝突(一致)しない値を主キー値として記録します。
- 新規に作成した書籍モデルインスタンスをデータベースに保存するとき、`default`で指定した`uuid.uuid4`関数を呼び出し、自動的に`id`フィールドに値を設定します。
- `null=True`と設定されているフィールドは、フィールド値が空であることを許容します。
  - データが空であることを、データベースでは`NULL`、`Python`では`None`と表現します。
- `blank=True`と設定されているフィールドは、後述するモデルフォームの対応するフィールドが未入力であることを許容します。
- 部署アプリの部署モデルを参照する`department`外部キーフィールドがあります。
- `auto_now_add=True`を指定した`registered_at`フィールドは、書籍がデータベースに登録されたとき、登録した日時をマイクロ秒単位で自動的に記録します。
- `auto_now=True`を指定した`updated_at`フィールドは、データベースに記録された書籍が更新されたとき、更新された日時をマイクロ秒単位で自動的に記録します。
- 書籍モデルインスタンスを`books`テーブルに記録するように、`db_table`で指定しています。

なお、`Django`は、日時を世界標準時(`UTC`)でデータベースに記録します。
後述するテンプレートに日時をレンダリングするとき、`Django`は世界標準時の日時を`プロジェクト設定ファイル`に設定された`TIME_ZONE`のタイムゾーンに変換した日時を出力します。

```python
# books.models.py
import uuid

from django.db import models

from departments.models import Department


class Book(models.Model):
    """書籍モデルクラス"""
    id = models.UUIDField('ID', primary_key=True, default=uuid.uuid4)
    name = models.CharField('書籍名', max_length=100)
    publisher = models.CharField('出版社', max_length=100, null=True,
                                 blank=True)
    isbn = models.CharField('ISBN', max_length=30, null=True, blank=True)
    published_at = models.DateField('発売日', null=True, blank=True)
    price = models.IntegerField('税抜き価格')
    department = models.ForeignKey(Department, db_column='部署ID',
                                   on_delete=models.CASCADE,
                                   verbose_name='部署')
    registered_at = models.DateTimeField('更新日時', auto_now_add=True)
    updated_at = models.DateTimeField('更新日時', auto_now=True)

    def __str__(self) -> str:
        """書籍名を返却する。

        Returns:
            書籍名。
        """
        return self.name

    class Meta:
        db_table = 'books'  # データベーステーブル名。
        verbose_name = verbose_name_plural = '書籍'

```

### 13.3. 書籍テーブルの作成

書籍アプリのモデルでマイグレーションファイルを作成して、データベースをマイグレートします。
データベースをマイグレートすると、データベースに`books`テーブルが作成されます。

```powershell
python manage.py makemigrations books   # マイグレーションファイル作成
python manage.py sqlmigrate books 0001  # マイグレート内容確認
python manage.py migrate books 0001     # データベースをマイグレート
```

![書籍アプリのマイグレート](images/migrate_books.png)

### 13.4. 書籍登録ページの作成

書籍を登録するページを下記の通り作成します。

#### 13.4.1. 書籍登録ビューの実装

書籍を登録するビューを`books/views.py`ファイルに下記の通り実装します。
`books/views.py`に記述されているソースコードを下記と入れ替えてください。

書籍登録ビューは、`Django`が提供する`CreateView`クラスから派生しています。
`CreateView`クラスは`ジェネリックビュー`と呼ばれ、モデルインスタンスを登録する際に、
一般的に必要となる機能を提供します。

書籍登録ビューの実装内容を以下に示します。

- クラス変数`model`で`CreateView`ビューが扱うモデルを書籍モデルに設定しています。
- クラス変数`fields`で、書籍登録ビューで入力するフィールドを指定しています。
- クラス変数`model`及び`fields`から、書籍の属性を入力するフォームが自動的に作成されます。
- `get_form`メソッドで、書籍の`ID`を入力するフィールドを`HTML`に表示しないようにしています。
  - `<input type="hidden">`として、`UUID4`文字列が入力された隠されたフィールドとして`HTML`がレンダリングされるようにしてます。
- 部署の入力は必須であるため、`get_form`メソッドで部署の未入力項目を表示しないようにしています。
- `get_context_data`メソッドで、テンプレートに描画するデータを設定しています。
- 書籍の属性が送信(POST)されたとき、自動的に書籍の属性を検証します。
  - 検証内容は、カスタマイズできます。
- 書籍の属性の検証に成功した場合、自動的にデータベースに書籍を登録します。

```python
# books/views.py
from typing import Dict, Type

from django import forms
from django.views import generic

from .models import Book


class BookCreateView(generic.CreateView):
    """書籍登録ビュー"""
    model = Book
    fields = ('id', 'name', 'publisher', 'isbn', 'published_at', 'price',
              'department',)

    def get_form(self, form_class=None) -> forms.Form:
        """書籍登録ビューのフォームを返却する。

        Args:
            form_class: フォームクラス。
            
        Returns:
            書籍登録ビューのフォーム。
        """
        form = super().get_form(form_class)
        form.fields['id'].widget = forms.HiddenInput()  # 隠されたフィールドに設定
        form.fields['department'].empty_label = None    # 未入力項目を非表示
        return form

    def get_context_data(self, **kwargs: Dict) -> Dict:
        """テンプレートをレンダリングするときに使用するデータを格納した辞書を返却する。

        Args:
            **kwargs: コンテキストに登録するデータを格納した辞書。
            
        Returns:
            テンプレートをレンダリングするときに使用するデータを格納した辞書。
        """
        ctx = super().get_context_data(**kwargs)
        ctx['submit_name'] = 'register'
        ctx['submit_value'] = '登録'
        return ctx
```

#### 13.4.2. Djangoによるテンプレートの検索

`Django`は、リクエストに対するレスポンスとなる`HTML`をテンプレートをレンダリングすることで生成します。なお、`Django`は`HTML`以外のテキストもレスポンスとして生成できます。

`Django`は、`HTML`などのテキストのテンプレートを、下記ディレクトリから検索します。

- `bookshelves/setting.py`ファイルの`TEMPLATES`辞書配列の要素の`DIRS`リストに指定されたディレクトリ
- `bookshelves/setting.py`ファイルの`TEMPLATES`辞書配列の要素の`APP_DIRS`が`True`の場合、モデルが属するアプリのディレクトリのテンプレートディレクトリ(`books/templates/books/`ディレクトリなど）

#### 13.4.3. 書籍登録ページのテンプレートの実装

`Django`が提供する`CreateView`クラスは、デフォルトで`<モデル名>_form.html`ファイルをテンプレートとして使用します。
よって、書籍登録ページのテンプレートファイル名のデフォルトは`book_form.html`です。
なお、テンプレートファイル名は変更可能です。

`books/templates/books`ディレクトリを作成します。

```powershell
mkdir -p books/templates/books
```

`books/templates/books/book_form.html`ファイルを作成して、書籍登録ページのテンプレートを以下の通り実装します。

- `Django`におけるテンプレートタグある`csrf_token`は、`クロスサイトリクエストフォージェリ`と呼ばれる攻撃を予防するために指定しています。
  - `csrf_token`テンプレートタグは、ランダムな長い文字列を生成して、書籍を登録するリクエストが、書籍登録ページから送信されたことを識別して、悪意のあるサイトからのリクエストを拒否することで、攻撃を防ぎます。
  - `Django`は、デフォルトで`CSRF`攻撃を防止する文字列が記録されていない`POST`リクエストを拒否します。
- `form.non_field_errors`は、フォームの単独のフィールドに関連しない、または複数のフィールドに関連するエラーをレンダリングします。
  - フィールドに関連するエラーは、そのフィールドにレンダリングされます。
- `form.as_p`により書籍の属性を入力するフォームがレンダリングされます。
  - たたし、`BookCreateView`クラスの`get_form`メソッドで`id`フィールドは表示されません(HTMLには記録されています)。
- `<input type="submit">`の`name`属性と`value`属性に、`BookCreateView`クラスの`get_context_data`メソッドで追加したデータをレンダリングします。
  - `{{ submit_name }}`は、`register`に置換されます。
  - `{{ submit_value }}`は、`登録`に置換されます。

```html
<!-- books/templates/book_form.html -->
<html lang="ja">
<head>
  <meta charset="utf-8">
</head>
<body>
  <!-- タイトル -->
  <h1>書籍の{{ submit_value }}</h1>
  <!-- フォーム -->
  <form method="post">
    {% csrf_token %}
    {{ form.non_field_errors }}
    {{ form.as_p }}
    <input type="submit" name="{{ submit_name }}" value="{{ submit_value }}">
  </form>
</body>
</html>
```

レンダリングされた実際の書籍登録ページのHTMLを整形した結果を下記に示します。

```html
<html lang="ja">
<head>
  <meta charset="utf-8">
</head>
<body>
  <h1>書籍の登録</h1>
  <form method="post">
    <input type="hidden" name="csrfmiddlewaretoken"
        value="K8p2kkO7DGE86lcSFZUVMPvrOXpxnydYjajT1FJst56O0mUPDZlyiqssZCGpu3Qf">
    <p>
      <label for="id_name">書籍名:</label>
      <input type="text" name="name" maxlength="100" required id="id_name">
    </p>
    <p>
      <label for="id_publisher">出版社:</label>
      <input type="text" name="publisher" maxlength="100" id="id_publisher">
    </p>
    <p>
      <label for="id_isbn">ISBN:</label>
      <input type="text" name="isbn" maxlength="30" id="id_isbn">
    </p>
    <p>
      <label for="id_published_at">発売日:</label>
      <input type="text" name="published_at" id="id_published_at">
    </p>
    <p>
      <label for="id_price">税抜き価格:</label>
      <input type="number" name="price" required id="id_price">
    </p>
    <p>
      <label for="id_department">部署:</label>
      <select name="department" required id="id_department">
        <option value="1">交通基盤計画部</option>
        <option value="2">橋梁構造部</option>
        <option value="3">河川・地域環境部</option>
        <option value="4">海外部</option>
        <option value="5">ICT開発室</option>
      </select>
    </p>
    <input type="hidden" name="id" value="ebaae338-d2bd-4024-97e1-e3d5598a08e0" id="id_id">
    <input type="hidden" name="initial-id" value="ebaae338-d2bd-4024-97e1-e3d5598a08e0" id="initial-id_id">
    <input type="submit" name="register" value="登録">
  </form>
</body>
</html>
```

#### 13.4.4. 書籍登録ページのURI実装

`books/urls.py`ファイルを作成して、書籍登録ページの`URI`を設定します。

```python
from django.urls import path

from . import views

app_name = 'books'

urlpatterns = [
    path('create/', views.BookCreateView.as_view(), name='book-create'),
]
```

#### 13.4.5. ルートURL設定に書籍アプリのURIを登録

`Django`では、`bookshelves/urls.py`を`ルートURL設定`ファイルと呼びます。
`bookshelves/urls.py`を下記の通り編集して、`ルートURL設定`に書籍アプリのURIを登録します。

これにより、書籍登録ページの`URI`が`books/create/`に設定されました。

```python
# bookshelves/urls.py
  from django.contrib import admin
- from django.urls import path
+ from django.urls import path, include
  
  urlpatterns = [
      path('admin/', admin.site.urls),
+     path('books/', include('books.urls')),    # booksアプリのurlsを含める
  ]
```

#### 13.4.6. 書籍登録ページの表示

`Django`開発用`Web`サーバーを起動(`python manage.py runserver`)して、`http://127.0.0.1:8000/books/create/` にWebブラウザでアクセスします。

書籍の属性を入力して、`登録`ボタンをクリックしてください。
日付は`2020-10-15`のように`yyyy-mm-dd`の書式で入力する必要があります。
なお、日付の入力書式は変更可能です。

![書籍登録ページ](images/book_create_page.png)

`登録`ボタンをクリックした後、`ImproperlyConfigured at /books/create/`のようなエラーが発生しますが、書籍はデータベースに登録されています。
このエラーは、書籍登録後に遷移するページを設定していないため発生しています。
書籍登録後に遷移するページは、後で作成します。

![書籍テーブル](images/books_table_row.png)

### 13.5. 書籍リストページの作成

書籍をリスト表示するページを下記の通り作成します。

#### 13.5.1. 書籍リストビューの実装

書籍のリストを表示するビューを`books/views.py`ファイルに下記の通り実装します。
書籍リストビューは、`Django`の`ジェネリックビュー`である`TemplateView`から派生させて実装しています。

`Django`の`ListView`クラスは、モデルインスタンスをリスト表示するために一般的に必要となる機能を提供しています。

- テンプレートにレンダリングするモデルインスタンスリストの取得
- 表示するモデルインスタンスが多い場合に、ページ分割(ページネーション)して表示する機能

書籍リストビューの実装内容を以下に示します。

- クラス変数`model`で`ListView`が扱うモデルを書籍モデルに設定しています。

```python
# books/views.py

+ class BoolListView(generic.ListView):
+     """書籍リストビュー"""
+     model = Book

```

#### 13.5.2. 書籍リストビューテンプレートの実装

`Django`の`ListView`クラスは、デフォルトで`<モデル名>_list.html`ファイルをテンプレートとして使用します。
よって、書籍リストビューテンプレートのファイル名は`book_list.html`です。
なお、テンプレートファイル名は変更可能です。

`books/templates/book_list.html`ファイルを作成して、書籍リストビューのテンプレートを以下の通り実装します。

- 書籍リストページの上部にブレットクラム(ページの位置を視覚的に表示。別名パンくずリスト)を配置しています。
- 書籍のリストは、`ListView`が継承する`MultipleObjectMixin`の`get_queryset`メソッドで取得されます。
- 書籍のリストが、テーブル形式でレンダリングしています。
- 書籍のリストは、コンテキスト`object_list`で`ListView`から渡されます。
- 書籍を表示したテーブルの下に、書籍登録ページに遷移するリンクを`Django`が提供する`url`テンプレートタグを利用して設置しています。

```html
<!-- books/templates/book_list.html -->
<html lang="ja">
<head>
  <meta charset="utf-8">
</head>
<body>
  <!-- ブレットクラム -->
  <div>書籍リスト</div>
  <!-- タイトル-->
  <h1>書籍の一覧</h1>
  <!-- 書籍リストテーブル -->
  <table border="1">
    <thead>
      <tr>
        <th>書籍名</th>
        <th>出版社</th>
        <th>ISBN</th>
        <th>発売日</th>
        <th>税抜き価格</th>
        <th>部署</th>
      </tr>
    </thead>
    <tbody>
      {% for book in object_list %}
      <tr>
        <td>{{ book.name }}</td>
        <td>{{ book.publisher }}</td>
        <td>{{ book.isbn }}</td>
        <td>{{ book.published_at }}</td>
        <td>{{ book.price }}</td>
        <td>{{ book.department }}</td>
      </tr>
      {% endfor %}
    </tbody>
  </table>
  <a href="{% url 'books:book-create' %}">登録</a>
</body>
</html>
```

#### 13.5.3. 書籍リストページのURI設定

`books/urls.py`を下記の通り編集します。
書籍リストページの`URI`は`books/`に設定されます。

```python
# books/urls.py
  urlpatterns = [
+     path('', views.BookListView.as_view(), name='book-list'),
      path('create/', views.BookCreateView.as_view(), name='book-create'),
  ]
```

![書籍リストページ](images/book_list_page.png)

### 13.6. 書籍登録ページのブレットクラム設定

書籍リストページを実装したため、書籍登録ページにブレットクラムを配置します。
`books/templates/books/book_form.html`を下記の通り編集します。

```html
<!-- books/templates/books/book_form.html -->
  <body>
+   <!-- ブレットクラム-->
+   <div>
+     <a href="{% url 'books:book-list' %}">書籍リスト</a> |  書籍{{ submit_name }}
+   </div>
    <!-- タイトル -->
    <h1>書籍の登録</h1>
    <form method="post">
```

![ブレットクラム付き書籍登録ページ](images/book_create_page_with_breadcrumb.png)

### 13.7. 書籍詳細ページの作成

書籍の属性を表示する書籍詳細ページを下記の通り作成します。

#### 13.7.1. 書籍詳細ビューの実装

書籍の属性を表示するビューを`books/views.py`に下記の通り実装します。
書籍詳細ビューは、`Django`の`ジェネリックビュー`である`DetailView`クラスから派生させて実装しています。

`Django`の`DetailView`は、モデルインスタンスの属性を表示するために、一般的に必要となる機能を提供しています。

- 表示するモデルインスタンスの取得

書籍詳細ビューの実装内容を以下に示します。

- クラス変数`model`で`DetailView`が扱うモデルを書籍モデルに設定しています。

```python
# books/views.py
+ class BookDetailView(generic.DetailView):
+     """書籍詳細ビュー"""
+     model = Book
+
```

#### 13.7.2. 書籍詳細ビューテンプレートの実装

`Django`の`DetailView`クラスは、デフォルトで`<モデル名>_detail.html`ファイルをテンプレートとして使用します。
よって、書籍詳細ビューテンプレートのファイル名は`book_detail.html`です。

`books/templates/book_detail.html`ファイルを作成して、書籍詳細ビューテンプレートを以下の通り実装します。

- 書籍詳細ページの上部にブレットクラムを配置しています。
- `Django`の`DetailView`クラスは、テンプレートにレンダリングするモデルインスタンスをコンテキスト`object`で、テンプレートをレンダリングするオブジェクトに渡します。

```html
<!-- books/templates/books/book_detail.html -->
<html lang="ja">
<head>
  <meta charset="utf-8">
</head>
<body>
  <!-- ブレットクラム-->
  <div>
    <a href="{% url 'books:book-list' %}">書籍リスト</a> |  書籍詳細
  </div>
  <!-- タイトル -->
  <h1>書籍の詳細</h1>
  <!-- 書籍の属性 -->
  <p><label>書籍名:</label> {{ object.name }}</p>
  <p><label>出版社:</label> {{ object.publisher }}</p>
  <p><label>ISBN:</label> {{ object.isbn }}</p>
  <p><label>発売日:</label> {{ object.published_at }}</p>
  <p><label>税抜き価格:</label> {{ object.price }}</p>
  <p><label>部署:</label> {{ object.department }}</p>
  <p><label>登録日時:</label> {{ object.registered_at }}</p>
  <p><label>更新日時:</label> {{ object.updated_at }}</p>
</body>
</html>
```

#### 13.7.3. 書籍詳細ページのURI設定

`books/urls.py`を下記の通り編集します。
書籍詳細ページの`URI`は`books/detail/<uuid>/`に設定されます。
書籍の属性を表示するために、属性表示する書籍を特定する主キーの値を`<uuid>`で指定します。

下記`<uuid:pk>`の部分は、`Django`において`パスコンバーター`と呼ばれます。
`パスコンバーター`は、ビューを呼び出すとき、`URI`から値を取り出して、ビューに渡します。
下記では、`URI`の`UUID`文字列が取り出されて、ビューに渡されます。
ビューやテンプレートでは`パスコンバーター`に指定されたキーワード`pk`で参照できます。

```python
# books/urls.py
  urlpatterns = [
      path('', views.BookListView.as_view(), name='book-list'),
+     path('detail/<uuid:pk>/', views.BookDetailView.as_view(), name='book-detail'),
      path('create/', views.BookCreateView.as_view(), name='book-create'),
  ]
```

### 13.8. 書籍登録後に遷移するページの設定

書籍登録ページで書籍を登録した後、エラーが発生していました。
これは、書籍登録後に遷移するページを設定していないために発生したエラーでした。
ここで、書籍登録ページで書籍登録後、書籍詳細ページに遷移するように設定します。
`books/views.py`を下記の通り編集してください。
`reverse`関数には、書籍詳細ページで属性を表示する書籍の`ID`を渡しています。

なお、`CreateView`クラスでは、クライアントから`POST`リクエストを受け取り、フォームに入力された内容の検証に成功後、`object`インスタンス変数に、登録するモデルインスタンスが設定されます。

```python
# books/views.py
  from typing import Dict, Type
  
+ from django.urls import reverse
  from django import forms
  from django.views import generic
  
  from .models import Book
....
# BookCreateViewクラス内
+     def get_success_url(self) -> str:
+         """書籍登録後に遷移するページのURIを返却する。
+
+         Returns:
+             書籍登録後に遷移するページのURI。
+         """
+         return reverse('books:book-detail', kwargs={'pk': self.object.id})
```

書籍登録ページで書籍を登録後、登録した書籍の属性を表示する書籍詳細ページに遷移することを確認してください。

### 13.9. 書籍リストページに書籍詳細ページへのリンクを設置

書籍リストページに書籍詳細ページへのリンクを設置します。
`books/templates/books/book_list.html`を下記の通り編集します。

`url`テンプレートタグに、`for`ループ内で取得した書籍モデルインスタンスの`ID`を渡しています。

```html
<!-- books/templates/books/book_list.html -->
    <thead>
      <tr>
        <th>書籍名</th>
        <th>出版社</th>
        <th>ISBN</th>
        <th>発売日</th>
        <th>税抜き価格</th>
        <th>部署</th>
+       <th />
      </tr>
    </thead>
    <tbody>
      {% for book in object_list %}
      <tr>
        <td>{{ book.name }}</td>
        <td>{{ book.publisher }}</td>
        <td>{{ book.isbn }}</td>
        <td>{{ book.published_at }}</td>
        <td>{{ book.price }}</td>
        <td>{{ book.department }}</td>
+       <td><a href="{% url 'books:book-detail' book.id %}">詳細</a></td>
      </tr>
      {% endfor %}
    </tbody>
```

ブラウザで`http://127.0.0.1:8000/books/`にアクセスして、書籍リストページで
`詳細`リンクをクリックして書籍詳細ページに遷移するか確認してください。

![書籍詳細ページへのリンクを設置した書籍リストページ](images/book_list_page_with_detail_link.png)

![書籍詳細ページ](images/book_detail_page.png)

### 13.10. 書籍更新ページの作成

書籍の属性を更新する書籍更新ページを下記の通り作成します。

#### 13.10.1. 書籍更新ビューの実装

書籍の属性を更新するビューを`books/views.py`に下記の通り実装します。
書籍更新ビューは、`Django`の`ジェネリックビュー`である`UpdateView`クラスから派生させて実装しています。

`Django`の`UpdateView`は、モデルインスタンスの属性を更新するために、一般的に必要となる機能を提供しています。

- 属性を入力するフォームの作成
- フォームに入力されたデータの検証
- 検証成功後のモデルインスタンスの更新

書籍更新ビューの実装内容を以下に示します。

- `BookCreateView`クラスと異なり、書籍のIDを更新しないため、クラス変数`fields`に`id`フィールドを指定していません。
- `BookUpdateView`クラスは、デフォルトで`BookCreateView`クラスと同じテンプレートを使用します。
- テンプレートをレンダリングするオブジェクトに渡す`コンテキスト`を更新用に設定しています。
- 書籍を更新した後、書籍詳細ページに遷移するように設定しています。

```python
# books/views.py
class BookUpdateView(generic.UpdateView):
    """書籍更新ビュー"""
    model = Book
    fields = ('name', 'publisher', 'isbn', 'published_at', 'price',
              'department',)

    def get_form(self, form_class=None) -> forms.Form:
        """書籍登録ビューのフォームを返却する。

        Args:
            form_class: フォームクラス。
            
        Returns:
            書籍登録ビューのフォーム。
        """
        form = super().get_form(form_class)
        form.fields['department'].empty_label = None    # 未入力項目を非表
        return form

    def get_context_data(self, **kwargs: Dict) -> Dict:
        """テンプレートをレンダリングするときに使用するデータを格納した辞書を
           返却する。

        Args:
            **kwargs: コンテキストに登録するデータを格納した辞書。
            
        Returns:
            テンプレートをレンダリングするときに使用するデータを格納した辞書。
        """
        ctx = super().get_context_data(**kwargs)
        ctx['submit_name'] = 'update'
        ctx['submit_value'] = '更新'
        return ctx

    def get_success_url(self) -> str:
        """書籍更新後に遷移するページのURIを返却する。

        Returns:
            書籍更新後に遷移するページのURI。
        """
        return reverse('books:book-detail', kwargs={'pk': self.object.id}
```

#### 13.10.3. 書籍更新ページのURI設定

`books/urls.py`を下記の通り編集します。
書籍更新ページの`URI`は`books/update/<uuid>/`に設定されます。

```python
# books/urls.py
  urlpatterns = [
      path('', views.BookListView.as_view(), name='book-list'),
      path('detail/<uuid:pk>/', views.BookDetailView.as_view(), name='book-detail'),
+     path('update/<uuid:pk>/', views.BookUpdateView.as_view(), name='book-update'),
      path('create/', views.BookCreateView.as_view(), name='book-create'),
  ]
```

### 13.11. 書籍更新ページに遷移するリンクの設置

書籍更新ページに遷移するリンクを、書籍詳細ページに設置します。
`books/templates/books/book_detail`ファイルを下記の通り編集して下さい。

```html
<!-- books/templates/books/book_detail.html -->
    <p><label>登録日時:</label> {{ object.registered_at }}</p>
    <p><label>更新日時:</label> {{ object.updated_at }}</p>
+   <a href="{% url 'books:book-update' object.id %}">更新</a>
  </body>
  </html>
```

### 13.12. 書籍を削除する機能の実装

#### 13.12.1. 書籍削除ビューの実装

書籍を削除するビューを`books/views.py`に下記の通り実装します。
書籍削除ビューは、`Django`の`ジェネリックビュー`である`DeleteView`クラスから派生させて実装しています。

- `success_url`クラス変数に書籍削除後に書籍リストページに遷移するように設定しています。
  - `reverse_lazy`関数は、`reverse`関数と異なり`URI`を遅延評価します。
  - クラス変数に値を設定する場合、`reverse`関数を使用できません。
    - `Python`のクラス変数を評価するタイミングや、`Django`が`URI`を評価するタイミングなど、内部的な説明が必要になるため省略します。

```python
# books/views.py
  from typing import Dict, Type
  
- from django.urls import reverse
+ from django.urls import reverse, reverse_lazy
  from django import forms
  from django.views import generic
...
+ class BookDeleteView(generic.DeleteView):
+     """書籍削除ビュー"""
+     model = Book
+     success_url = reverse_lazy('books:book-list')
+
```

#### 13.12.2. 書籍削除テンプレートの実装

`Django`の`DeleteView`クラスは、デフォルトで`<モデル名>_confirm_delete.html`ファイルをテンプレートとして使用します。
よって、書籍削除ビューテンプレートのファイル名は`book_confirm_delete.html`です。

`books/templates/books/book_confirm_delete.html`ファイルを作成して、書籍削除ビューテンプレートを以下の通り実装します。

```html
<html lang="ja">
<head>
  <meta charset="utf-8">
</head>
<body>
  <!-- ブレットクラム-->
  <div>
    <a href="{% url 'books:book-list' %}">書籍リスト</a> |  書籍削除
  </div>
  <!-- タイトル -->
  <h1>書籍の削除</h1>
  <!-- 書籍の属性 -->
  <p><label>書籍名:</label> {{ object.name }}</p>
  <p><label>出版社:</label> {{ object.publisher }}</p>
  <p><label>ISBN:</label> {{ object.isbn }}</p>
  <p><label>発売日:</label> {{ object.published_at }}</p>
  <p><label>税抜き価格:</label> {{ object.price }}</p>
  <p><label>部署:</label> {{ object.department }}</p>
  <p><label>登録日時:</label> {{ object.registered_at }}</p>
  <p><label>更新日時:</label> {{ object.updated_at }}</p>
  <form method="post">
    {% csrf_token %}
    <p>この書籍を削除しますか?</p>
    <input type="submit" value="削除">
  </form>
</body>
</html>
```

#### 13.12.3. 書籍削除ページのURI設定

`books/urls.py`を下記の通り編集します。
書籍更新ページの`URI`は`books/delete/<uuid>/`に設定されます。

```python
# books/urls.py
  urlpatterns = [
      path('', views.BookListView.as_view(), name='book-list'),
      path('detail/<uuid:pk>/', views.BookDetailView.as_view(), name='book-detail'),
      path('update/<uuid:pk>/', views.BookUpdateView.as_view(), name='book-update'),
+     path('delete/<uuid:pk>/', views.BookDeleteView.as_view(), name='book-delete'),
      path('create/', views.BookCreateView.as_view(), name='book-create'),
  ]
```

#### 13.13. 書籍削除ページへのリンクの設置

書籍削除ページに遷移するリンクを書籍リストページに設置します。
`books/templates/books/book_list.html`ファイルを下記の通り編集してください。

```html
<!-- books/templates/books/book_list.html -->
    <thead>
      <tr>
        <th>書籍名</th>
        <th>出版社</th>
        <th>ISBN</th>
        <th>発売日</th>
        <th>税抜き価格</th>
        <th>部署</th>
        <th />
+       <th />
      </tr>
    </thead>
    <tbody>
      {% for book in object_list %}
      <tr>
        <td>{{ book.name }}</td>
        <td>{{ book.publisher }}</td>
        <td>{{ book.isbn }}</td>
        <td>{{ book.published_at }}</td>
        <td>{{ book.price }}</td>
        <td>{{ book.department }}</td>
        <td><a href="{% url 'books:book-detail' book.id %}">詳細</a></td>
+       <td><a href="{% url 'books:book-delete' book.id %}">削除</a></td>
      </tr>
      {% endfor %}
    </tbody>
```

書籍リストページの`削除`リンクをクリックして、書籍削除ページに遷移するとともに、書籍削除後、書籍リストページが表示されることを確認してください。

以上で、モデルを参照、更新、削除及び登録する一連の機能を提供する書籍管理アプリを実装しました。

最後に、モデルインスタンスを管理するために、`Django`が提供する`管理サイト`を説明します。

## 14. 管理サイト

`管理サイト`は`Django`が提供するモデルインスタンスを管理するためのサイトです。
管理サイトはデフォルトで有効になっていますが、スタッフ権限を持つユーザーのみアクセスできるサイトです。

管理サイトの`URI`は、`ルートURL設定ファイル(bookshelves/urls.py)`にすでに設定されています。

```python
# bookshelves/urls.py
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),        # 管理サイトのURI
    path('books/', include('books.urls')),

```

ブラウザで`http://127.0.0.1:8000/admin/`にアクセスすると、管理サイトを利用するユーザーを認証するページが表示されます。

![管理サイトログインページ](images/login_page_at_admin_site.png)

### 14.1. スーパーユーザーの作成

スタッフ権限をもつスーパーユーザー(`django`)を下記の通り`createsuperuser`コマンドで作成します。

なお、作成したユーザーの属性は、プロジェクトで使用するデータベースに記録されます。
ユーザーの属性を記録するテーブルは、最初のデータベースのマイグレートで作成されています。

![スーパーユーザーの作成](images/createsuperuser.png)

短いパスワードを入力した場合は、上記の通り警告されます。
短いパスワードを使用する場合は、`y`を押下して、パスワードの検証を回避してユーザーを作成してください。

ブラウザで`http://127.0.0.1:8000/admin/`にアクセスした後、`createsuperuser`コマンドで入力したユーザー名とパスワードを入力して`ログイン`ボタンをクリックすると`管理サイト`にログインできます。

現時点では、`管理サイト`で管理するモデルを指定していないため、`Django`がデフォルトで実装するユーザーモデルとグループモデルが表示されます。
`[認証と認可]-[ユーザー]`リンクをクリックすると、先ほど作成したユーザーが表示されます。

### 14.2. 部署モデルの登録

部署モデルを管理サイトに登録します。
`departments/admin.py`ファイルを開き、下記の通り編集してください。

```python
# departments/admin.py
  from django.contrib import admin
+  
+ from .models import Department
+
+
- # Register your models here.
+ class DepartmentAdmin(admin.ModelAdmin):
+     fields = ('id', 'name',)
+     list_display = fields
+
+
+ admin.site.register(Department, DepartmentAdmin)
```

`管理サイト`をリロードすると、部署モデルが`管理サイト`に表示されます。

`管理サイト`のルート(`http://127.0.0.1:8000/admin)`の`部署`リンクをクリックすると部署モデルインスタンスを表示するページが表示されます。
このページでは、部署の更新、削除及び登録が可能です。

部署を更新する場合は、編集する部署の行のリンクをクリックすると、部署の属性を更新するページに遷移します。

部署を削除する場合は、削除する部署の行をチェックして、`[操作]`ドロップダウンから`[選択した部署の削除]`を選択して、右横の`[実行]`ボタンをクリックします。

部署を登録する場合は、右上の`[部署を追加]`ボタンをクリックします。

![管理サイトにおける部署モデルの操作](images/departments_at_admin_site.png)

### 14.3. 書籍モデルの登録

書籍モデルを管理サイトに登録します。
`books/admin.py`ファイルを開き、下記の通り編集してください。

```python
# books/admin.py
  from django.contrib import admin
+  
+ from .models import Book
+
+
- # Register your models here.
+ class BookAdmin(admin.ModelAdmin):
+     fields = ('name', 'publisher', 'isbn', 'published_at', 'price',
+               'department',)
+     list_display = fields + ('registered_at', 'updated_at',)
+
+
+ admin.site.register(Book, BookAdmin)
```

`管理サイト`をリロードすると、書籍モデルが`管理サイト`に表示されます。

## 15. 最後に

以上で、`Django`による`Web`アプリ開発の説明を終わります。

実際に`Web`アプリをインターネットで公開するためには、`HTTP`プロトコル、`CSS`、`JavaScript`や`Web`サーバーなど様々な事を理解する必要がありますが、`Django`を使用することで意外と少ない作業で開発できることを実感できたと思います。

今回開発した`Web`アプリは、社内や個人で使用するには十分だと思います。
また、ビューやテンプレートなどを実装することを避けたい場合は、モデルのみを定義して`管理サイト`でデータを管理できます。

今回の説明を通じて`Web`アプリ開発に興味をもっていただければ幸いです。
