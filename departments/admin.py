from django.contrib import admin

from .models import Department


class DepartmentAdmin(admin.ModelAdmin):
    fields = ('id', 'name',)
    list_display = fields


admin.site.register(Department, DepartmentAdmin)
