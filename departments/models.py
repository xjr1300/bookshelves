from django.db import models


class Department(models.Model):
    """部署モデルクラス"""
    name = models.CharField('部署名', max_length=80)

    class Meta:
        verbose_name = verbose_name_plural = '部署'
        ordering = ('id',)

    def __str__(self) -> str:
        """部署名を返却する。

        Returns:
            部署名。
        """
        return self.name
